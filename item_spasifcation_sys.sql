-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema iss
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema iss
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `iss` DEFAULT CHARACTER SET utf8 ;
-- -----------------------------------------------------
-- Schema shop
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema shop
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `shop` DEFAULT CHARACTER SET utf8 ;
USE `iss` ;

-- -----------------------------------------------------
-- Table `shop`.`categories`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `shop`.`categories` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(255) NOT NULL,
  `Description` TEXT NOT NULL,
  `parent` INT(11) NOT NULL,
  `Ordering` INT(11) NULL DEFAULT NULL,
  `Visibility` TINYINT(4) NOT NULL DEFAULT '0',
  `Allow_Comment` TINYINT(4) NOT NULL DEFAULT '0',
  `Allow_Ads` TINYINT(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE INDEX `Name` (`Name` ASC),
  INDEX `perant_id_idx` (`parent` ASC),
  CONSTRAINT `perant_id`
    FOREIGN KEY (`parent`)
    REFERENCES `shop`.`categories` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 18
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `iss`.`User`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `iss`.`User` (
  `UserID` INT NOT NULL AUTO_INCREMENT,
  `Username` VARCHAR(45) NULL,
  `Password` VARCHAR(255) NULL,
  `Status` VARCHAR(45) NULL,
  PRIMARY KEY (`UserID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `iss`.`item`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `iss`.`item` (
  `itemid` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `owner` INT NOT NULL,
  `create_date` DATETIME NULL,
  PRIMARY KEY (`itemid`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC),
  INDEX `ownerID_idx` (`owner` ASC),
  CONSTRAINT `ownerID`
    FOREIGN KEY (`owner`)
    REFERENCES `iss`.`User` (`UserID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `iss`.`items_categories`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `iss`.`items_categories` (
  `iditems_categories` INT NOT NULL AUTO_INCREMENT,
  `itemsId` INT NOT NULL,
  `categoriesId` INT NOT NULL,
  PRIMARY KEY (`iditems_categories`),
  INDEX `categorie_idx` (`categoriesId` ASC),
  INDEX `item_idx` (`itemsId` ASC),
  CONSTRAINT `categorie`
    FOREIGN KEY (`categoriesId`)
    REFERENCES `shop`.`categories` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `item`
    FOREIGN KEY (`itemsId`)
    REFERENCES `iss`.`item` (`itemid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `iss`.`properties`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `iss`.`properties` (
  `propertiesId` INT NOT NULL AUTO_INCREMENT,
  `propertiesDataType` VARCHAR(45) NOT NULL,
  `propertiesName` VARCHAR(45) NOT NULL,
  `propertiescDisc` VARCHAR(255) NULL,
  PRIMARY KEY (`propertiesId`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `iss`.`properties_categories`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `iss`.`properties_categories` (
  `propertiesId` INT NOT NULL,
  `categoriesId` INT NOT NULL,
  `ID` INT NULL AUTO_INCREMENT,
  PRIMARY KEY (`propertiesId`, `categoriesId`),
  INDEX `categoriesId_idx` (`categoriesId` ASC),
  UNIQUE INDEX `index3` (`ID` ASC),
  CONSTRAINT `categoriesId`
    FOREIGN KEY (`categoriesId`)
    REFERENCES `shop`.`categories` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `propertiesId`
    FOREIGN KEY (`propertiesId`)
    REFERENCES `iss`.`properties` (`propertiesId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `iss`.`Test`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `iss`.`Test` (
  `idTest` INT NOT NULL AUTO_INCREMENT,
  `TestCraeteDate` DATE NOT NULL,
  `Tester` INT NOT NULL,
  `TestItemID` INT NOT NULL,
  `TestName` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idTest`),
  INDEX `TestItemId_idx` (`TestItemID` ASC),
  INDEX `TesterId_idx` (`Tester` ASC),
  CONSTRAINT `TesterId`
    FOREIGN KEY (`Tester`)
    REFERENCES `iss`.`User` (`UserID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `TestItemId`
    FOREIGN KEY (`TestItemID`)
    REFERENCES `iss`.`item` (`itemid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `iss`.`Result`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `iss`.`Result` (
  `TestID` INT NOT NULL,
  `ItemPropertiesID` INT NOT NULL,
  `Value` VARCHAR(45) NULL,
  PRIMARY KEY (`TestID`, `ItemPropertiesID`),
  INDEX `propertiesId_idx` (`ItemPropertiesID` ASC),
  CONSTRAINT `ResultTestId`
    FOREIGN KEY (`TestID`)
    REFERENCES `iss`.`Test` (`idTest`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `ResultpropertiesId`
    FOREIGN KEY (`ItemPropertiesID`)
    REFERENCES `iss`.`properties_categories` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

USE `shop` ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
